#!/bin/sh
IMAGE_ID=$(docker build -q ./scripts 2>/dev/null)
docker run --rm -it -e CARGO_HOME=/app/.cargo -v $(pwd):/app -w /app $IMAGE_ID "$@"
