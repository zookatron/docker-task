#!/bin/sh
docker compose -p docker_task -f ./scripts/docker-compose.yml up --abort-on-container-exit --build
docker compose -p docker_task -f ./scripts/docker-compose.yml rm -fsv
