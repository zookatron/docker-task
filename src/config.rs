use anyhow::{bail, Result};
use serde::Deserialize;
use serde_yaml::Value;
use std::collections::HashMap;
use std::env;
use std::path::PathBuf;
use tokio::fs;

pub type Config = HashMap<String, Task>;

#[derive(Debug, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum StringOrNumber {
    String(String),
    Number(i64),
}

structstruck::strike! {
    #[strikethrough[derive(Debug, PartialEq, Deserialize)]]
    #[strikethrough[serde(deny_unknown_fields)]]
    pub struct Task {
        pub annotations: Option<pub enum TaskAnnotations {
            #![serde(untagged)]
            List(Vec<String>),
            Map(HashMap<String, String>),
        }>,
        pub attach: Option<bool>,
        pub build: Option<pub enum TaskBuild {
            #![allow(clippy::large_enum_variant)]
            #![serde(untagged)]
            Simple(String),
            Complex(pub struct TaskBuildObject {
                pub additional_contexts: Option<pub enum TaskBuildAdditionalContexts {
                    #![serde(untagged)]
                    List(Vec<String>),
                    Map(HashMap<String, String>),
                }>,
                pub args: Option<pub enum TaskBuildArgs {
                    #![serde(untagged)]
                    List(Vec<String>),
                    Map(HashMap<String, StringOrNumber>),
                }>,
                pub cache_from: Option<Vec<String>>,
                pub cache_to: Option<Vec<String>>,
                pub context: Option<String>,
                pub dockerfile: Option<String>,
                pub dockerfile_inline: Option<String>,
                pub extra_hosts: Option<pub enum TaskBuildExtraHosts {
                    #![serde(untagged)]
                    List(Vec<String>),
                    Map(HashMap<String, String>),
                }>,
                pub isolation: Option<String>,
                pub labels: Option<pub enum TaskBuildLabels {
                    #![serde(untagged)]
                    List(Vec<String>),
                    Map(HashMap<String, StringOrNumber>),
                }>,
                pub network: Option<String>,
                pub no_cache: Option<bool>,
                pub platforms: Option<Vec<String>>,
                pub privileged: Option<bool>,
                pub pull: Option<bool>,
                pub secrets: Option<Vec<pub enum TaskBuildSecret {
                    #![serde(untagged)]
                    Simple(String),
                    Complex(pub struct TaskBuildSecretObject {
                        pub source: String,
                        pub target: Option<String>,
                        pub uid: Option<StringOrNumber>,
                        pub gid: Option<StringOrNumber>,
                        pub mode: Option<StringOrNumber>,
                    }),
                }>>,
                pub shm_size: Option<StringOrNumber>,
                pub ssh: Option<pub enum TaskBuildSsh {
                    #![serde(untagged)]
                    List(Vec<String>),
                    Map(HashMap<String, String>),
                }>,
                pub tags: Option<Vec<String>>,
                pub target: Option<String>,
            }),
        }>,
        pub blkio_config: Option<Value>, // blkio_config is not supported
        pub cpu_count: Option<u64>,
        pub cpu_percent: Option<u8>,
        pub cpu_shares: Option<StringOrNumber>,
        pub cpu_period: Option<StringOrNumber>,
        pub cpu_quota: Option<StringOrNumber>,
        pub cpu_rt_runtime: Option<StringOrNumber>,
        pub cpu_rt_period: Option<StringOrNumber>,
        pub cpuset: Option<String>,
        pub cap_add: Option<Vec<String>>,
        pub cap_drop: Option<Vec<String>>,
        pub cgroup: Option<pub enum TaskCGroup {
            #![serde(rename_all = "lowercase")]
            Host,
            Private,
        }>,
        pub cgroup_parent: Option<String>,
        pub command: Option<pub enum TaskCommand {
            #![serde(untagged)]
            Single(String),
            List(Vec<String>),
        }>,
        pub configs: Option<Vec<pub enum TaskConfig {
            #![serde(untagged)]
            Simple(String),
            Complex(pub struct TaskConfigObject {
                pub source: String,
                pub target: Option<String>,
                pub uid: Option<StringOrNumber>,
                pub gid: Option<StringOrNumber>,
                pub mode: Option<StringOrNumber>,
            }),
        }>>,
        pub container_name: Option<String>,
        pub credential_spec: Option<Value>, // credential_spec is not supported
        pub depends_on: Option<Value>, // depends_on is not supported
        pub deploy: Option<pub struct TaskDeploy {
            pub endpoint_mode: Option<pub enum TaskDeployEndpointMode {
                #![serde(rename_all = "lowercase")]
                Vip,
                DnsRR,
            }>,
            pub labels: Option<pub enum TaskDeployLabels {
                #![serde(untagged)]
                List(Vec<String>),
                Map(HashMap<String, StringOrNumber>),
            }>,
            pub mode: Option<pub enum TaskDeployMode {
                #![serde(rename_all = "lowercase")]
                Global,
                Replicated,
            }>,
            pub placement: Option<pub struct TaskDeployPlacement {
                constraints: Option<Vec<String>>,
                preferences: Option<pub enum TaskDeployPlacementPreferences {
                    #![serde(untagged)]
                    List(Vec<String>),
                    Map(HashMap<String, String>),
                }>,
                max_replicas_per_node: u64,
            }>,
            pub replicas: u64,
            pub resources: Option<pub struct TaskDeployResources {
                limits: Option<pub struct TaskDeployResourceLimits {
                    cpus: Option<StringOrNumber>,
                    memory: Option<String>,
                    pids: Option<u64>,
                }>,
                reservations: Option<pub struct TaskDeployResourceReservations {
                    cpus: Option<StringOrNumber>,
                    memory: Option<String>,
                    generic_resources: Option<Vec<pub struct TaskDeployReservationGenericResources {
                        discrete_resource_spec: Option<pub struct TaskDeployReservationGenericResourceSpec {
                            kind: Option<String>,
                            value: Option<i64>,
                        }>,
                    }>>,
                    devices: Option<Vec<pub struct TaskDeployReservationDevices {
                        capabilities: Option<Vec<String>>,
                        count: Option<StringOrNumber>,
                        device_ids: Option<Vec<String>>,
                        driver: Option<String>,
                        options: Option<HashMap<String, Value>>,
                    }>>,
                }>,
            }>,
            pub restart_policy: Option<pub struct TaskDeployRestartPolicy {
                condition: Option<pub enum TaskDeployRestartPolicyCondition {
                    #![serde(rename_all = "kebab-case")]
                    None,
                    OnFailure,
                    Any,
                }>,
                delay: Option<String>,
                max_attempts: Option<u64>,
                window: Option<String>,
            }>,
            rollback_config: Option<Vec<pub struct TaskDeployRollbackConfig {
                parallelism: Option<u64>,
                delay: Option<String>,
                failure_action: Option<pub enum TaskDeployRollbackConfigFailureAction {
                    #![serde(rename_all = "lowercase")]
                    Continue,
                    Pause,
                }>,
                monitor: Option<String>,
                max_failure_ratio: Option<u64>,
                order: Option<pub enum TaskDeployRollbackConfigOrder {
                    #![serde(rename_all = "kebab-case")]
                    StopFirst,
                    StartFirst,
                }>,
            }>>,
            update_config: Option<Vec<pub struct TaskDeployUpdateConfig {
                parallelism: Option<u64>,
                delay: Option<String>,
                failure_action: Option<pub enum TaskDeployUpdateConfigFailureAction {
                    #![serde(rename_all = "lowercase")]
                    Continue,
                    Pause,
                }>,
                monitor: Option<String>,
                max_failure_ratio: Option<u64>,
                order: Option<pub enum TaskDeployUpdateConfigOrder {
                    #![serde(rename_all = "kebab-case")]
                    StopFirst,
                    StartFirst,
                }>,
            }>>,
        }>,
        pub device_cgroup_rules: Option<Vec<String>>,
        pub devices: Option<Vec<String>>,
        pub dns: Option<pub enum TaskDns {
            #![serde(untagged)]
            Single(String),
            List(Vec<String>),
        }>,
        pub dns_opt: Option<Vec<String>>,
        pub dns_search: Option<pub enum TaskDnsSearch {
            #![serde(untagged)]
            Single(String),
            List(Vec<String>),
        }>,
        pub domainname: Option<String>,
        pub entrypoint: Option<pub enum TaskEntrypoint {
            #![serde(untagged)]
            Single(String),
            List(Vec<String>),
        }>,
        pub env_file: Option<pub enum TaskEnvFiles {
            #![serde(untagged)]
            Single(String),
            List(Vec<String>),
        }>,
        pub environment: Option<pub enum TaskBuildEnvironment {
            #![serde(untagged)]
            List(Vec<String>),
            Map(HashMap<String, StringOrNumber>),
        }>,
        pub expose: Option<Vec<StringOrNumber>>,
        pub extends: Option<pub enum TaskExtends {
            Simple(String),
            Complex(pub struct TaskExtendsObject {
                pub task: String,
                pub file: Option<String>,
            }),
        }>,
        pub external_links: Option<Vec<String>>,
        pub extra_hosts: Option<pub enum TaskExtraHosts {
            #![serde(untagged)]
            List(Vec<String>),
            Map(HashMap<String, String>),
        }>,
        pub group_add: Option<Vec<StringOrNumber>>,
        pub healthcheck: Option<HashMap<String, Value>>, // healthcheck is not supported
        pub hostname: Option<String>,
        pub image: Option<String>,
        pub init: Option<bool>,
        pub ipc: Option<String>,
        pub isolation: Option<String>,
        pub labels: Option<pub enum TaskLabels {
            #![serde(untagged)]
            List(Vec<String>),
            Map(HashMap<String, StringOrNumber>),
        }>,
        pub links: Option<Vec<String>>,
        pub logging: Option<pub struct TaskLogging {
            pub driver: Option<String>,
            pub options: Option<HashMap<String, Value>>,
        }>,
        pub network_mode: Option<String>,
        pub networks: Option<pub enum TaskNetworks {
            #![serde(untagged)]
            List(Vec<String>),
            Map(HashMap<String, pub struct TaskNetwork {
                pub aliases: Option<Vec<String>>,
                pub ipv4_address: Option<String>,
                pub ipv6_address: Option<String>,
                pub link_local_ips: Option<Vec<String>>,
                pub priority: Option<i64>,
            }>),
        }>,
        pub mac_address: Option<String>,
        pub mem_swappiness: Option<u8>,
        pub memswap_limit: Option<StringOrNumber>,
        pub oom_kill_disable: Option<bool>,
        pub oom_score_adj: Option<i64>,
        pub pid: Option<Value>, // pid is not supported
        pub platform: Option<String>,
        pub ports: Option<Vec<pub enum TaskPort {
            #![serde(untagged)]
            Simple(StringOrNumber),
            Complex(pub struct TaskPortObject {
                pub target: StringOrNumber,
                pub host_ip: Option<String>,
                pub published: Option<StringOrNumber>,
                pub protocol: Option<pub enum TaskPortProtocol {
                    #![serde(rename_all = "lowercase")]
                    Tcp,
                    Udp,
                }>,
                pub mode: Option<pub enum TaskPortMode {
                    #![serde(rename_all = "lowercase")]
                    Host,
                    Ingress,
                }>,
            }),
        }>>,
        pub privileged: Option<bool>,
        pub profiles: Option<Vec<String>>,
        pub pull_policy: Option<pub enum TaskPullPolicy {
            #![serde(rename_all = "snake_case")]
            Always,
            Never,
            IfNotPresent,
            Missing,
            Build,
        }>,
        pub read_only: Option<bool>,
        pub restart: Option<pub enum TaskRestart {
            #![serde(rename_all = "kebab-case")]
            No,
            Always,
            OnFailure,
            UnlessStopped,
        }>,
        pub runtime: Option<String>,
        pub script: Option<Vec<String>>,
        pub secrets: Option<Vec<pub enum TaskSecret {
            #![serde(untagged)]
            Simple(String),
            Complex(pub struct TaskSecretObject {
                pub source: String,
                pub target: Option<String>,
                pub uid: Option<StringOrNumber>,
                pub gid: Option<StringOrNumber>,
                pub mode: Option<StringOrNumber>,
            }),
        }>>,
        pub security_opt: Option<Vec<String>>,
        pub shm_size: Option<StringOrNumber>,
        pub stdin_open: Option<bool>,
        pub stop_grace_period: Option<String>,
        pub stop_signal: Option<String>,
        pub storage_opt: Option<HashMap<String, StringOrNumber>>,
        pub sysctls: Option<pub enum TaskBuildSysctls {
            #![serde(untagged)]
            List(Vec<String>),
            Map(HashMap<String, StringOrNumber>),
        }>,
        pub tmpfs: Option<pub enum TaskTmpFs {
            #![serde(untagged)]
            Single(String),
            List(Vec<String>),
        }>,
        pub tty: Option<bool>,
        pub ulimits: Option<HashMap<String, pub enum TaskUlimit {
            #![serde(untagged)]
            Value(i64),
            Bounds(pub struct TaskUlimitObject {
                pub hard: i64,
                pub soft: i64,
            }),
        }>>,
        pub user: Option<String>,
        pub userns_mode: Option<String>,
        pub uts: Option<pub enum TaskUts {
            #![serde(rename_all = "lowercase")]
            Host,
        }>,
        pub volumes: Option<Vec<pub enum TaskVolume {
            #![serde(untagged)]
            Simple(String),
            Complex(pub struct TaskVolumeObject {
                #[serde(rename = "type")]
                pub volume_type: pub enum TaskVolumeType {
                    #![serde(rename_all = "lowercase")]
                    Bind,
                    TmpFs,
                    NPipe,
                    Cluster,
                },
                pub source: Option<String>,
                pub target: Option<String>,
                pub read_only: Option<bool>,
                pub consistency: Option<String>,
                pub bind: Option<pub struct TaskVolumeBind {
                    pub propagation: Option<pub enum TaskVolumeBindPropagation {
                        #![serde(rename_all = "lowercase")]
                        RPrivate,
                        Private,
                        RShared,
                        Shared,
                        RSlave,
                        Slave,
                    }>,
                    pub create_host_path: Option<bool>,
                    pub selinux: Option<pub enum TaskVolumeBindSELinux {
                        #[serde(rename = "z")]
                        Shared,
                        #[serde(rename = "Z")]
                        Private,
                    }>,
                }>,
                pub volume: Option<pub struct TaskVolumeOptions {
                    pub nocopy: Option<bool>,
                }>,
                pub tmpfs: Option<pub struct TaskVolumeTmpFs {
                    pub size: Option<StringOrNumber>,
                    pub mode: Option<StringOrNumber>,
                }>,
            }),
        }>>,
        pub volumes_from: Option<Vec<String>>,
        pub working_dir: Option<String>,
    }
}

pub fn find_config() -> Option<PathBuf> {
    let mut path = env::current_dir().ok()?;
    loop {
        let file = path.join("docker-task.yml");
        if file.exists() {
            return Some(file);
        }
        if !path.pop() {
            break;
        }
    }
    None
}

pub async fn read_config(file: &PathBuf) -> Result<Config> {
    let config: Value = serde_yaml::from_str(&(fs::read_to_string(file).await?))?;
    check_config(serde_yaml::from_value::<Config>(interpolate_config(
        config,
    ))?)
}

fn interpolate_config(value: Value) -> Value {
    lazy_static::lazy_static! {
        static ref INTERPOLATOR: regex::Regex = regex::Regex::new(r"(?:(?:[^\\]|^)\$\{([^}]*(?:\\}[^}]*)*[^\\])\})|(?:(?:[^\\]|^)\$([^{ ][^ ]*))").unwrap();
        static ref ENV_VARS: HashMap<String, String> = env::vars().collect::<HashMap<_, _>>();
    }
    match value {
        Value::String(string) => Value::String(
            INTERPOLATOR
                .replace_all(&string, |caps: &regex::Captures| {
                    caps.get(1)
                        .or(caps.get(2))
                        .map(|var| {
                            ENV_VARS
                                .get(var.as_str())
                                .map(String::as_str)
                                .unwrap_or_default()
                                .to_string()
                        })
                        .unwrap_or_default()
                })
                .to_string(),
        ),
        Value::Sequence(sequence) => Value::Sequence(
            sequence
                .into_iter()
                .map(interpolate_config)
                .collect::<Vec<_>>(),
        ),
        Value::Mapping(mapping) => Value::Mapping(
            mapping
                .into_iter()
                .map(|(key, value)| (key, interpolate_config(value)))
                .collect::<serde_yaml::Mapping>(),
        ),
        Value::Tagged(tagged) => Value::Tagged(Box::new(serde_yaml::value::TaggedValue {
            tag: tagged.tag,
            value: interpolate_config(tagged.value),
        })),
        _ => value,
    }
}

fn check_config(config: Config) -> Result<Config> {
    for task in config.values() {
        if task.annotations.is_some() {
            bail!("annotations is not supported");
        }
        if task.attach.is_some() {
            bail!("attach is not supported");
        }
        if task.blkio_config.is_some() {
            bail!("blkio_config is not supported");
        }
        if task.cpu_count.is_some() {
            bail!("cpu_count is not supported");
        }
        if task.cpu_percent.is_some() {
            bail!("cpu_percent is not supported");
        }
        if task.cpu_shares.is_some() {
            bail!("cpu_shares is not supported");
        }
        if task.cpu_period.is_some() {
            bail!("cpu_period is not supported");
        }
        if task.cpu_quota.is_some() {
            bail!("cpu_quota is not supported");
        }
        if task.cpu_rt_runtime.is_some() {
            bail!("cpu_rt_runtime is not supported");
        }
        if task.cpu_rt_period.is_some() {
            bail!("cpu_rt_period is not supported");
        }
        if task.cpuset.is_some() {
            bail!("cpuset is not supported");
        }
        if task.cap_add.is_some() {
            bail!("cap_add is not supported");
        }
        if task.cap_drop.is_some() {
            bail!("cap_drop is not supported");
        }
        if task.cgroup.is_some() {
            bail!("cgroup is not supported");
        }
        if task.cgroup_parent.is_some() {
            bail!("cgroup_parent is not supported");
        }
        if task.command.is_some() {
            bail!("command is not supported");
        }
        if task.configs.is_some() {
            bail!("configs is not supported");
        }
        if task.container_name.is_some() {
            bail!("container_name is not supported");
        }
        if task.credential_spec.is_some() {
            bail!("credential_spec is not supported");
        }
        if task.depends_on.is_some() {
            bail!("depends_on is not supported");
        }
        if task.deploy.is_some() {
            bail!("deploy is not supported");
        }
        if task.device_cgroup_rules.is_some() {
            bail!("device_cgroup_rules is not supported");
        }
        if task.devices.is_some() {
            bail!("devices is not supported");
        }
        if task.dns.is_some() {
            bail!("dns is not supported");
        }
        if task.dns_opt.is_some() {
            bail!("dns_opt is not supported");
        }
        if task.dns_search.is_some() {
            bail!("dns_search is not supported");
        }
        if task.domainname.is_some() {
            bail!("domainname is not supported");
        }
        if task.entrypoint.is_some() {
            bail!("entrypoint is not supported");
        }
        if task.env_file.is_some() {
            bail!("env_file is not supported");
        }
        if task.environment.is_some() {
            bail!("environment is not supported");
        }
        if task.expose.is_some() {
            bail!("expose is not supported");
        }
        if task.extends.is_some() {
            bail!("extends is not supported");
        }
        if task.external_links.is_some() {
            bail!("external_links is not supported");
        }
        if task.extra_hosts.is_some() {
            bail!("extra_hosts is not supported");
        }
        if task.group_add.is_some() {
            bail!("group_add is not supported");
        }
        if task.healthcheck.is_some() {
            bail!("healthcheck is not supported");
        }
        if task.hostname.is_some() {
            bail!("hostname is not supported");
        }
        if task.ipc.is_some() {
            bail!("ipc is not supported");
        }
        if task.isolation.is_some() {
            bail!("isolation is not supported");
        }
        if task.labels.is_some() {
            bail!("labels is not supported");
        }
        if task.links.is_some() {
            bail!("links is not supported");
        }
        if task.logging.is_some() {
            bail!("logging is not supported");
        }
        if task.network_mode.is_some() {
            bail!("network_mode is not supported");
        }
        if task.networks.is_some() {
            bail!("networks is not supported");
        }
        if task.mac_address.is_some() {
            bail!("mac_address is not supported");
        }
        if task.mem_swappiness.is_some() {
            bail!("mem_swappiness is not supported");
        }
        if task.memswap_limit.is_some() {
            bail!("memswap_limit is not supported");
        }
        if task.oom_kill_disable.is_some() {
            bail!("oom_kill_disable is not supported");
        }
        if task.oom_score_adj.is_some() {
            bail!("oom_score_adj is not supported");
        }
        if task.pid.is_some() {
            bail!("pid is not supported");
        }
        if task.platform.is_some() {
            bail!("platform is not supported");
        }
        if task.ports.is_some() {
            bail!("ports is not supported");
        }
        if task.privileged.is_some() {
            bail!("privileged is not supported");
        }
        if task.profiles.is_some() {
            bail!("profiles is not supported");
        }
        if task.pull_policy.is_some() {
            bail!("pull_policy is not supported");
        }
        if task.read_only.is_some() {
            bail!("read_only is not supported");
        }
        if task.restart.is_some() {
            bail!("restart is not supported");
        }
        if task.runtime.is_some() {
            bail!("runtime is not supported");
        }
        if task.secrets.is_some() {
            bail!("secrets is not supported");
        }
        if task.security_opt.is_some() {
            bail!("security_opt is not supported");
        }
        if task.shm_size.is_some() {
            bail!("shm_size is not supported");
        }
        if task.stdin_open.is_some() {
            bail!("stdin_open is not supported");
        }
        if task.stop_grace_period.is_some() {
            bail!("stop_grace_period is not supported");
        }
        if task.stop_signal.is_some() {
            bail!("stop_signal is not supported");
        }
        if task.storage_opt.is_some() {
            bail!("storage_opt is not supported");
        }
        if task.sysctls.is_some() {
            bail!("sysctls is not supported");
        }
        if task.tmpfs.is_some() {
            bail!("tmpfs is not supported");
        }
        if task.ulimits.is_some() {
            bail!("ulimits is not supported");
        }
        if task.userns_mode.is_some() {
            bail!("userns_mode is not supported");
        }
        if task.uts.is_some() {
            bail!("uts is not supported");
        }
        if task.volumes_from.is_some() {
            bail!("volumes_from is not supported");
        }
        if let Some(TaskBuild::Complex(build)) = &task.build {
            if build.additional_contexts.is_some() {
                bail!("build.additional_contexts is not supported");
            }
            if build.args.is_some() {
                bail!("build.args is not supported");
            }
            if build.cache_from.is_some() {
                bail!("build.cache_from is not supported");
            }
            if build.cache_to.is_some() {
                bail!("build.cache_to is not supported");
            }
            if build.extra_hosts.is_some() {
                bail!("build.extra_hosts is not supported");
            }
            if build.isolation.is_some() {
                bail!("build.isolation is not supported");
            }
            if build.labels.is_some() {
                bail!("build.labels is not supported");
            }
            if build.network.is_some() {
                bail!("build.network is not supported");
            }
            if build.no_cache.is_some() {
                bail!("build.no_cache is not supported");
            }
            if build.no_cache.is_some() {
                bail!("build.no_cache is not supported");
            }
            if build.platforms.is_some() {
                bail!("build.platforms is not supported");
            }
            if build.privileged.is_some() {
                bail!("build.privileged is not supported");
            }
            if build.pull.is_some() {
                bail!("build.pull is not supported");
            }
            if build.secrets.is_some() {
                bail!("build.secrets is not supported");
            }
            if build.shm_size.is_some() {
                bail!("build.shm_size is not supported");
            }
            if build.ssh.is_some() {
                bail!("build.ssh is not supported");
            }
            if build.tags.is_some() {
                bail!("build.tags is not supported");
            }
            if build.target.is_some() {
                bail!("build.target is not supported");
            }
        }
        if let Some(volumes) = &task.volumes {
            for item in volumes {
                if let TaskVolume::Complex(volume) = item {
                    if volume.source.is_some() {
                        bail!("volumes.*.source is not supported");
                    }
                    if volume.target.is_some() {
                        bail!("volumes.*.target is not supported");
                    }
                    if volume.read_only.is_some() {
                        bail!("volumes.*.read_only is not supported");
                    }
                    if volume.consistency.is_some() {
                        bail!("volumes.*.consistency is not supported");
                    }
                    if volume.bind.is_some() {
                        bail!("volumes.*.bind is not supported");
                    }
                    if volume.volume.is_some() {
                        bail!("volumes.*.volume is not supported");
                    }
                    if volume.tmpfs.is_some() {
                        bail!("volumes.*.tmpfs is not supported");
                    }
                    bail!("volumes.*.type is not supported");
                }
            }
        }
    }
    Ok(config)
}
