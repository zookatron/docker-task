use anyhow::{anyhow, Result};
use core::time::Duration;
use crossterm::{
    event::{Event, EventStream, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode},
};
use docker_api::{
    api::container::Container,
    conn::TtyChunk,
    errors::Error,
    models::{ImageBuildChunk, ProgressDetail},
    opts::{ContainerCreateOpts, ContainerStopOpts, ImageFilter, ImageListOpts, PullOpts},
    Docker,
};
use futures::{
    future::ready,
    io::AsyncWriteExt,
    stream::{once, select, StreamExt},
};
use http::status::StatusCode;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use std::collections::HashMap;
use std::io::{stderr, stdout, Write};

lazy_static::lazy_static! {
    static ref DOCKER: Docker = Docker::unix("/var/run/docker.sock");
}

struct DockerImage<'a> {
    name: &'a str,
    tag: &'a str,
}

fn normalize_image(image: &str) -> DockerImage {
    let mut image_parts = image.split(':');
    DockerImage {
        name: image_parts.next().unwrap(),
        tag: image_parts.next().unwrap_or("latest"),
    }
}

pub async fn get_image_with_tag(image: &str) -> Result<Option<String>> {
    let normalized = normalize_image(image);
    let normalized_str = format!("{}:{}", normalized.name, normalized.tag);
    let mut images = DOCKER
        .images()
        .list(
            &ImageListOpts::builder()
                .filter([ImageFilter::Reference(
                    normalized.name.to_string(),
                    Some(normalized.tag.to_string()),
                )])
                .build(),
        )
        .await?
        .into_iter()
        .filter(|image| image.repo_tags.iter().any(|tag| *tag == normalized_str))
        .collect::<Vec<_>>();
    match images.len() {
        0 => Ok(None),
        1 => Ok(Some(images.remove(0).id)),
        _ => Err(anyhow!("Multiple images found for \"{normalized_str}\"!")),
    }
}

pub async fn pull_image(image: &str) -> Result<()> {
    let normalized = normalize_image(image);
    let images = DOCKER.images();
    let mut stream = images.pull(
        &PullOpts::builder()
            .image(normalized.name)
            .tag(normalized.tag)
            .build(),
    );
    let progress = MultiProgress::new();
    let style = ProgressStyle::with_template(
        "{prefix:12} {bar:40.cyan/blue} {bytes:10}/{total_bytes:10} {msg}",
    )
    .unwrap()
    .progress_chars("#>-");
    let mut bars = HashMap::<String, ProgressBar>::new();
    while let Some(pull_result) = stream.next().await {
        if let ImageBuildChunk::PullStatus {
            id: Some(id),
            status,
            progress_detail,
            ..
        } = pull_result?
        {
            if id != normalized.tag {
                let bar = bars.entry(id.clone()).or_insert_with(|| {
                    let new_bar = progress.add(ProgressBar::new(0));
                    new_bar.set_style(style.clone());
                    new_bar.set_prefix(id);
                    new_bar
                });
                if status == "Pull complete" {
                    bar.finish_with_message(status);
                } else {
                    bar.set_message(status);
                    let details = progress_detail.unwrap_or(ProgressDetail {
                        current: None,
                        total: None,
                    });
                    if let Some(current) = details.current {
                        bar.set_position(current);
                    }
                    if let Some(total) = details.total {
                        bar.set_length(total);
                    }
                }
            }
        }
    }
    progress.clear()?;
    Ok(())
}

pub async fn ensure_image(image: &str) -> Result<String> {
    match get_image_with_tag(image).await? {
        Some(id) => Ok(id),
        None => {
            pull_image(image).await?;
            get_image_with_tag(image)
                .await?
                .ok_or(anyhow!("Unable to find pulled image for \"{image}\"!"))
        }
    }
}

async fn safe_stop_container(container: &Container) -> Result<()> {
    let result = container
        .stop(
            &ContainerStopOpts::builder()
                .wait(Duration::from_secs(1))
                .build(),
        )
        .await;
    if let Err(Error::Fault { code, .. }) = result {
        match code {
            StatusCode::NOT_MODIFIED => (),
            _ => result?,
        }
    }
    Ok(())
}

fn crossterm_event_to_bytes(event: Event) -> Vec<u8> {
    match event {
        Event::Key(key) => match key.code {
            KeyCode::Char(code) => vec![code as u8],
            KeyCode::Enter => b"\r".to_vec(),
            KeyCode::Backspace => b"\x7F".to_vec(),
            KeyCode::Left => b"\x1b\x5b\x44".to_vec(),
            KeyCode::Right => b"\x1b\x5b\x43".to_vec(),
            KeyCode::Up => b"\x1b\x5b\x41".to_vec(),
            KeyCode::Down => b"\x1b\x5b\x42".to_vec(),
            KeyCode::Home => b"\x1b\x5b\x48".to_vec(),
            KeyCode::End => b"\x1b\x5b\x46".to_vec(),
            KeyCode::PageUp => b"\x1b\x5b\x35\x7e".to_vec(),
            KeyCode::PageDown => b"\x1b\x5b\x36\x7e".to_vec(),
            KeyCode::Tab => b"\x09".to_vec(),
            KeyCode::BackTab => b"\x1b\x5b\x5a".to_vec(),
            KeyCode::Delete => b"\x1b\x5b\x33\x7e".to_vec(),
            KeyCode::Insert => b"\x1b\x5b\x32\x7e".to_vec(),
            KeyCode::Esc => b"\x1b".to_vec(),
            KeyCode::CapsLock => vec![],
            KeyCode::ScrollLock => vec![],
            KeyCode::NumLock => vec![],
            KeyCode::PrintScreen => vec![],
            KeyCode::Pause => vec![],
            KeyCode::Menu => vec![],
            KeyCode::KeypadBegin => vec![],
            KeyCode::F(_) => vec![],
            KeyCode::Media(_) => vec![],
            KeyCode::Modifier(_) => vec![],
            KeyCode::Null => vec![],
        },
        Event::FocusGained => vec![],
        Event::FocusLost => vec![],
        Event::Mouse(_) => vec![],
        Event::Paste(pasted) => pasted.into_bytes(),
        Event::Resize(_, _) => vec![],
    }
}

#[derive(Debug)]
enum IOEvent {
    Input(Event),
    Output(TtyChunk),
    End,
}

pub async fn run_image(opts: &ContainerCreateOpts) -> Result<()> {
    enable_raw_mode()?;
    let container = DOCKER.containers().create(opts).await?;
    let multiplexer = container.attach().await?;
    container.start().await?;
    let (reader, mut writer) = multiplexer.split();
    let output = reader
        .map(|item| Result::<_>::Ok(item.map(IOEvent::Output)?))
        .chain(once(ready(Ok(IOEvent::End))));
    let input = EventStream::new().map(|item| Result::<_>::Ok(item.map(IOEvent::Input)?));
    let mut events = select(input, output);
    let mut stdout = stdout();
    let mut stderr = stderr();
    while let Some(event) = events.next().await {
        match event? {
            IOEvent::Input(input) => {
                writer.write_all(&crossterm_event_to_bytes(input)).await?;
            }
            IOEvent::Output(TtyChunk::StdOut(bytes)) => {
                stdout.write_all(&bytes)?;
                stdout.flush()?;
            }
            IOEvent::Output(TtyChunk::StdErr(bytes)) => {
                stderr.write_all(&bytes)?;
                stderr.flush()?;
            }
            IOEvent::Output(TtyChunk::StdIn(_)) => unreachable!(),
            IOEvent::End => break,
        }
    }
    safe_stop_container(&container).await?;
    container.remove(&Default::default()).await?;
    disable_raw_mode()?;
    Ok(())
}
