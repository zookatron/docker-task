use anyhow::{anyhow, Context, Result};
use clap::{Arg, ArgAction, Command};
use std::env;
use std::ffi::OsString;
use std::os::unix::fs::PermissionsExt;
use std::path::{Path, PathBuf};
use std::process::ExitCode;
use tokio::fs;

mod config;
mod docker;

fn cli() -> Command {
    Command::new("docker task")
        .about("Docker task runner")
        .version("0.1.0")
        .override_usage("docker task [options] <task> [args...]")
        .subcommand_required(true)
        .arg_required_else_help(true)
        .allow_external_subcommands(true)
        .arg(
            Arg::new("file")
                .short('f')
                .long("file")
                .action(ArgAction::Set)
                .value_name("FILE")
                .help("Specify the path of the task configuration file to use.")
                .value_parser(clap::value_parser!(PathBuf)),
        )
        .subcommand(
            Command::new("docker-cli-plugin-metadata").about("Output the Docker plugin metadata"),
        )
}

#[tokio::main]
async fn main() -> ExitCode {
    let matches = match env::args().nth(1).as_deref() {
        Some("task") => cli().get_matches_from(env::args_os().skip(1)),
        _ => cli().get_matches(),
    };

    match matches.subcommand() {
        Some(("docker-cli-plugin-metadata", _)) => {
            println!(
                "{{\n\
                    \"SchemaVersion\": \"0.1.0\",\n\
                    \"Vendor\": \"Tim Zook\",\n\
                    \"Version\": \"0.1.0\",\n\
                    \"ShortDescription\": \"Docker task runner\",\n\
                    \"URL\": \"https://gitlab.com/zookatron/docker-task\"\n\
                }}",
            );
            ExitCode::SUCCESS
        }
        Some((task, arguments)) => {
            let args = arguments
                .get_many::<OsString>("")
                .into_iter()
                .flatten()
                .collect::<Vec<_>>();
            let file = matches.get_one::<PathBuf>("file");
            match run_task(file, task, &args).await {
                Ok(_) => ExitCode::SUCCESS,
                Err(error) => {
                    eprintln!("{error:#}");
                    ExitCode::FAILURE
                }
            }
        }
        _ => unreachable!(),
    }
}

async fn run_task(
    config_file: Option<&PathBuf>,
    task_name: &str,
    args: &[&OsString],
) -> Result<()> {
    let mut file = config_file.map(|file| file.to_owned()).or_else(config::find_config)
        .ok_or(anyhow!("No configuration file found (you need to create a docker-task.yml file in this folder or one of its parent folders)"))?;
    let config = config::read_config(&file).await.context(format!(
        "Unable to read configuration file \"{}\"",
        file.display()
    ))?;
    file.pop();
    run_task_with_data(
        &file,
        config.get(task_name).ok_or(anyhow!(
            "No task in configuration file \"{}\" named \"{}\"",
            file.display(),
            task_name
        ))?,
        args,
    )
    .await
}

async fn run_task_with_data(folder: &Path, task: &config::Task, _args: &[&OsString]) -> Result<()> {
    let image = docker::ensure_image(
        task.image
            .as_ref()
            .ok_or(anyhow!("You must provide an image value for the task"))?,
    )
    .await?;
    let user = task.user.clone().unwrap_or(format!(
        "{}:{}",
        nix::unistd::geteuid(),
        nix::unistd::getegid()
    ));
    let working_dir = task
        .working_dir
        .clone()
        .unwrap_or(folder.to_string_lossy().to_string());
    let user_volumes = task
        .volumes
        .as_ref()
        .map(|volumes| {
            volumes
                .iter()
                .flat_map(|volume| match volume {
                    config::TaskVolume::Simple(value) => Some(value.as_str()),
                    _ => None,
                })
                .collect::<Vec<_>>()
        })
        .unwrap_or_default();
    let xdg_dirs = xdg::BaseDirectories::with_prefix("docker_task").unwrap();
    let entrypoint = xdg_dirs.place_state_file(uuid::Uuid::new_v4().to_string())?;
    fs::write(
        &entrypoint,
        format!(
            "#!/bin/sh\n{}",
            task.script
                .as_ref()
                .map(|script| script.join("\n"))
                .unwrap_or_default()
        ),
    )
    .await?;
    fs::set_permissions(&entrypoint, std::fs::Permissions::from_mode(0o700)).await?;
    docker::run_image(
        &docker_api::opts::ContainerCreateOpts::builder()
            .image(image)
            .entrypoint(["/docker_task_entrypoint"])
            .volumes(
                user_volumes.iter().chain(
                    vec![
                        format!("{}:/docker_task_entrypoint", entrypoint.to_string_lossy())
                            .as_str(),
                        format!("{0}:{0}", folder.to_string_lossy()).as_str(),
                    ]
                    .iter(),
                ),
            )
            .working_dir(working_dir)
            .user(user)
            .init(task.init.unwrap_or(true))
            .tty(task.tty.unwrap_or(true))
            .attach_stdin(true)
            .attach_stdout(true)
            .attach_stderr(true)
            .build(),
    )
    .await?;
    fs::remove_file(&entrypoint).await?;
    Ok(())
}
